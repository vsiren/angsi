#!/bin/bash

if [ $DEPLOY_ENV = 'dev' ]
then
  sed -i 's/<placeholder>/http:\/\/tech-ants-console.test.za-tech.net/' /etc/nginx/conf.d/nginx_app.conf
  sed -i 's/<servername>/ants-console-tst.zhonganinfo.com/' /etc/nginx/conf.d/nginx_app.conf

elif [ $DEPLOY_ENV = 'test' ]
then
  sed -i 's/<placeholder>/http:\/\/tech-ants-console.test.za-tech.net/' /etc/nginx/conf.d/nginx_app.conf
  sed -i 's/<servername>/ants-console-tst.zhonganinfo.com/' /etc/nginx/conf.d/nginx_app.conf

elif [ $DEPLOY_ENV = 'test_public' ]
then
  sed -i 's/<placeholder>/http:\/\/tech-ants-console.test.za-tech.net/' /etc/nginx/conf.d/nginx_app.conf
  sed -i 's/<servername>/ants-console-tst.zhonganinfo.com/' /etc/nginx/conf.d/nginx_app.conf

elif [ $DEPLOY_ENV = 'pre' ]
then
  sed -i 's/<placeholder>/http:\/\/tech-ants-console.pre.za-tech.net/' /etc/nginx/conf.d/nginx_app.conf
  sed -i 's/<servername>/ants-console-pre.zhonganinfo.com/' /etc/nginx/conf.d/nginx_app.conf

elif [ $DEPLOY_ENV = 'prd' ]
then
  sed -i 's/<placeholder>/http:\/\/tech-ants-console.prd.za-tech.net/' /etc/nginx/conf.d/nginx_app.conf
  sed -i 's/<servername>/ants-console.zhonganinfo.com/' /etc/nginx/conf.d/nginx_app.conf

fi

nginx -g "daemon off"\;