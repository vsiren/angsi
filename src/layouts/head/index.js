import React from 'react';
import PropTypes, { func } from 'prop-types';
import router from 'umi/router';
import Link from 'umi/link';
import { connect } from 'dva';
import withRouter from 'umi/withRouter';
import { message } from 'antd';
import store from '../../../utils/store';
// import menuRoute from 'src/components/menu/menuRoute'
import api from '../../network/user';
import style from './head.less';
import getEnv from '../../../utils/env';

let DevHost = 'https://console-test.annchaincloud.net';
const Dict = {
  local: 'http://localhost:8000',
  dev: 'https://console.dev.annchaincloud.net',
  test: 'https://console.test.annchaincloud.net',
  uat: 'https://console.uat.annchaincloud.net',
  pre: 'https://console.pre.annchaincloud.net',
  prd: 'https://console.annchaincloud.net',
};
const HostDict = {
  local: 'https://www.prd.annchaincloud.net',
  dev: 'https://www.dev.annchaincloud.net',
  test: 'https://www.test.annchaincloud.net',
  uat: 'https://www.uat.annchaincloud.net',
  pre: 'https://www.pre.annchaincloud.net',
  prd: 'https://www.annchaincloud.net',
};

DevHost = Dict[getEnv()];
class header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: true,
      products: [],
      userInfo: {},
      money: 0.00,
      productShow: false,
      feeShow: false,
      personContentShow: false,
    };
  }

  componentDidMount() {
    this.setState({
      env: this.props.env,
    });
  }

  render() {
    return (
      <div className={style.head}>
        <div className={style.left} />
        <div className={style.right}>

          <div className={style.common}>

            <div
              className="personWrap"
              style={{ position: 'relative' }}
            >
              <i
                style={{ color: '', fontSize: 16 }}
                className="iconfont icontouxiang"
              />
              <div
                className={`${style.personContent} personContent`}
              >
                <PersonContent
                  {...this.props}
                  env={this.props.env}
                  userInfo={this.props.userInfo}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  // const { data } = state.menu;
  return {
    // data
  };
}
const locationHeader = withRouter(header);
export default connect(mapStateToProps)(locationHeader);


function PersonContent(prop) {
  console.log(prop, 444);

  const env = prop.env;
  return (
    <div>
      <div className={style.personContentTop}>
        <i />
        <span>管理员</span>
      </div>

      <div className={style.personContentBottom}>
        <div
          onClick={async () => {
            const serverName = store.getKey('serverName');
            api.logout({
              serverName,
              userInfo: prop.userInfo,
            });
          }}
        >
        退出管理台
        </div>
      </div>
    </div>
  );
}
