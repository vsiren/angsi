import React from 'react';
import withRouter from 'umi/withRouter';
import { connect } from 'dva';
import { Spin, Icon } from 'antd';
import {deepClone} from 'utils/util';
import Header from './head';
import Left from './left';
import { MenuContext, menu, menuConfig } from '../MenuContext';
import styles from './index.less';
import store from 'utils/store';
import { getConsoleCode } from '@/network/product';

const antIcon = <Icon type="loading" style={{ fontSize: 60 }} spin />;
// todo getConfig setConfig env
class LayoutPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      menus: [],
      ready: false
    };
  }

  setMenus(menus = []) {
    this.props.dispatch({
      type: 'global/setMenus',
      payload: {
        menus,
      },
    });
  }

  setTitle = (title) => {
    this.setState({
      title,
    });
  };

  updateMenu = () => {
    // 保存菜单context

    const conf = this.setMenuActive(menu);
    this.setState({ menu: conf }, () => {
      // 渲染菜单
      this.setMenus(conf);
    });
  };

  setMenuActive = (menu, deep = true) => {
    const cloneMenu = deep ? deepClone(menu) : menu.slice(0);
    console.log('active', cloneMenu);
    cloneMenu.every(m => {
      if (m.url === this.props.location.pathname) {
        m.active = true;
        return false;
      } if (m.children) {
        this.setMenuActive(m.children, false);
      }
      return true;
    });
    return cloneMenu;
  };

  componentDidMount() {
    this.setTitle('后台管理系统');
    this.updateMenu();
    if (this.props.location.pathname !== '/login') {
      getConsoleCode({}).then(res => {
        if (res.success) {
          store.setKey('code', res.data);
          this.setState({
            ready: true
          })
        }
      });
    }
  }


  layout = () => {
    if (this.props.location.pathname === '/login') {
      return (
        <div className={styles.userContent}>
          <div className={styles.content}>
            <div className={styles.main}>
              {this.props.children}
            </div>
          </div>
        </div>
      );
    }
    return (
      <React.Fragment>
        <div className={styles.header}>
          <Header />
          <div className={styles.content}>
            <div className={styles.left}>
              {this.props.menus.length ? (
                <Left title={this.state.title} menus={this.props.menus} />
              ) : (
                ''
              )}
            </div>

            <div className={styles.right}>
              { this.state.ready && this.props.children}
            </div>
          </div>
        </div>
        <div className="loadMask ant-modal-wrap" style={{ display: 'none' }} id="loadMask">
          <Spin indicator={antIcon} />
        </div>
      </React.Fragment>
    );
  };

  render() {
    const _this = this;
    return <React.Fragment>{_this.layout()}</React.Fragment>;
  }
}
function mapStateToProps(state) {
  if (state.global) {
    const { currentLanguage, time, userInfo, menus} = state.global;
    return {
      currentLanguage, time, menus, userInfo,
    };
  }
  return {};
}
const rootLayoutPage = withRouter(LayoutPage);
export default connect(mapStateToProps)(rootLayoutPage);
