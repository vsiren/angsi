import request from '../../utils/request';

// 前端额外添加，便于转发
const prefix = '/v1';
// 报表分页接口
export const getProductItemPage = (params) => request(`${prefix}/product/getProductItemPage`, {
  method: 'POST',
  data: params,
});
export const insertProductItem = (params) => request(`${prefix}/product/insertProductItem`, {
  method: 'POST',
  data: params,
});
export const modifyProductItem = (params) => request(`${prefix}/product/modifyProductItem`, {
  method: 'POST',
  data: params,
});
export const offShelfProductItem = (params) => request(`${prefix}/product/offShelfProductItem`, {
  method: 'POST',
  data: params,
});
export const onShelfProductItem = (params) => request(`${prefix}/product/onShelfProductItem`, {
  method: 'POST',
  data: params,
});
