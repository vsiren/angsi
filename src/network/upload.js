import request from '../../utils/request';


// 报表分页接口
export const uploadFile = (params) => request('/file/upload', {
  method: 'POST',
  data: params,
});
export const downloadFile = (params) => request(`/file/download/${params.key}`, {
  method: 'GET',
  data: params,
});
