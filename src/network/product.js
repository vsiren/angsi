import request from '../../utils/request';

// 前端额外添加，便于转发
const prefix = '/v1';
// 报表分页接口
export const getGoodsPage = (params) => request(`${prefix}/product/getProductPage`, {
  method: 'POST',
  data: params,
});
export const insertProduct = (params) => request(`${prefix}/product/insertProduct`, {
  method: 'POST',
  data: params,
});
export const modifyProduct = (params) => request(`${prefix}/product/modifyProduct`, {
  method: 'POST',
  data: params,
});
export const offShelfProduct = (params) => request(`${prefix}/product/offShelfProduct`, {
  method: 'POST',
  data: params,
});
export const onShelfProduct = (params) => request(`${prefix}/product/onShelfProduct`, {
  method: 'POST',
  data: params,
});
export const getConsoleCode = (params) => request(`${prefix}/code/getConsoleCode`, {
  method: 'GET',
  data: params,
});
export const getProductAll = (params) => request(`${prefix}/product/getProductAll`, {
  method: 'GET',
  data: params,
});
export const getProductItem = (params) => request(`${prefix}/product/getProductItem`, {
  method: 'POST',
  data: params,
});
