import request from '../../utils/request';

// 前端额外添加，便于转发
const prefix = '/v1';
// 报表分页接口
export const getProductMembershipPage = (params) => request(`${prefix}/productMembership/getProductMembershipPage`, {
  method: 'POST',
  data: params,
});
export const configProductMembership = (params) => request(`${prefix}/productMembership/configProductMembership`, {
  method: 'POST',
  data: params,
});
export const modifyProductMembership = (params) => request(`${prefix}/productMembership/modifyProductMembership`, {
  method: 'POST',
  data: params,
});
export const offShelfProductMembership = (params) => request(`${prefix}/productMembership/offShelfProductMembership`, {
  method: 'POST',
  data: params,
});
export const onShelfProductMembership = (params) => request(`${prefix}/productMembership/onShelfProductMembership`, {
  method: 'POST',
  data: params,
});
export const getProductMembershipItem = (params) => request(`${prefix}/productMembership/getProductMembershipItem`, {
  method: 'POST',
  data: params,
});
