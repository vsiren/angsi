import request from '../../utils/request';

// 前端额外添加，便于转发
const prefix = '/v1';
// 报表分页接口
export const getOrderByPage = (params) => request(`${prefix}/order/getOrderByPage`, {
  method: 'POST',
  data: params,
});
export const getOrderDetail = (params) => request(`${prefix}/order/getOrderDetail`, {
  method: 'POST',
  data: params,
});
export const orderRefund = (params) => request(`${prefix}/order/orderRefund`, {
  method: 'POST',
  data: params,
});
export const orderRetry = (params) => request(`${prefix}/order/orderRetry`, {
  method: 'POST',
  data: params,
});
