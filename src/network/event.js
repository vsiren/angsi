import request from '../../utils/request';

// 前端额外添加，便于转发
const prefix = '/v1';
// 报表分页接口
export const getEventPage = (params) => request(`${prefix}/event/getEventPage`, {
  method: 'POST',
  data: params,
});
export const insertEvent = (params) => request(`${prefix}/event/insertEvent`, {
  method: 'POST',
  data: params,
});
export const modifyEvent = (params) => request(`${prefix}/event/modifyEvent`, {
  method: 'POST',
  data: params,
});
export const offShelfEvent = (params) => request(`${prefix}/event/offShelfEvent`, {
  method: 'POST',
  data: params,
});
export const onShelfEvent = (params) => request(`${prefix}/event/onShelfEvent`, {
  method: 'POST',
  data: params,
});
