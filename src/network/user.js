import request from '../../utils/request'
// import eventRequest from '../../utils/eventRequest'
import getEnv from '../../utils/env'
import { message } from 'antd'
import store from '../../utils/store'
function getCurrentHost() {
  return window.location.protocol + '//' + window.location.host
}
const dict = {
  local: 'http://sso.anlink.tech',
  dev: 'http://zis-stargate-anlink-console-frontend.test.za-tech.net',
  prd: 'https://sso-prd.anlink.tech'
}
const host = {
  local: '',
  dev: 'http://zis-stargate-anlink-console-frontend.test.za-tech.net',
  prd: 'https://sso-prd.anlink.tech'
}

export default {
  async getUserInfo() {
    let env = getEnv();
    let url = host[env]
    let res = await request(`/tech-anlink-user/v2/anlink/userInfo/info`, {
      method: 'get'
    })
    return res && res.data && res.data || {};
  },
  async getProductList() {
    let env = getEnv();
    let url = host[env]
    // console.log(env,33333)
    // console.log(url,'url11111')
    let res = await request(`/tech-anlink-product/api/v1/product/getAninkProduct`, {
      method: 'get'
    })
    return res && res.data && res.data || {};
  },
  async getAccountMoney() {
    let env = getEnv();
    let url = host[env]
    let res = await request(`/tech-anlink-charge/v1/charging/acct/getAcctBalance`, {
      method: 'post'
    })
    return res && res.data && res.data || {};
  },
  async checkLogin(opt = { serverName: '' }) {
    console.log('checklogin', 666)
    console.log(`/${opt.serverName}/api/checkLogin`)
    let res = await request(`/${opt.serverName}/api/checkLogin`, {
      method: 'get'
    })
    return res && res.data || {};
  },
  async logout(opt = { serverName: '', userInfo: {} }) {
    let env = getEnv();
    // let url = host[env]
    let url = '';
    url = (env == 'dev' || env == 'local') ? 'https://sso-tst.anlink.tech' : 'https://sso.annchaincloud.net';
    let ssoUrl = store.getKey('ssoUrl');
    if (ssoUrl) {
      url = ssoUrl;
    }
    let res = await request(`/${opt.serverName}/api/logout`, {
      method: 'get'
    })
    if (opt.userInfo.userTagId === 1) {
      window.location.href = url + '/logout?type=za'
    } else {
      window.location.href = url + '/logout?service=' +
        encodeURIComponent(getCurrentHost() + '/' + opt.serverName + '/')
    }

    return res && res.data || {};
  },
  async getMessageList() {
    let res = await request(`/tech-anlink-console/message/notify/queryMessageNotifys`, {
      method:'POST',
      data: {
        isMarked: "0",
        pageNo: 1,
        pageSize: 10000000
      }
    })
    console.log(res, 333)
    return res;
  }
}