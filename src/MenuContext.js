/*
 * @Description: In User Settings Edit
 * @Author: your name
 * @Date: 2019-08-09 14:28:28
 * @LastEditTime: 2019-09-20 16:45:37
 * @LastEditors: Please set LastEditors
 */
import React from 'react';
import router from 'umi/router';

const menuIcons = {};
const menuIconsContext = require.context('@/assets/menu', true, /\.(png)$/);
menuIconsContext.keys().map(key => {
  menuIcons[key.replace(/\.\/(\w+)\.png/, '$1')] = menuIconsContext(key);
});
const analyse = require('@/assets/menu/analyse.png');
const link = require('@/assets/menu/link.png');
// name和图片名称一样
export const menuConfig = [
  {
    iconname: 'tool',
    title: '商品管理',
    url: '/product',
    router: () => {
      router.push('/product');
    },
  },

  {
    iconname: 'account',
    title: '常规活动管理',
    url: '/normalActivity',
    router: () => {
      router.push('/normalActivity');
    },
  },
  {
    iconname: 'amoeba',
    title: '会员折扣管理',
    url: '/discount',
    // show: true,
    router: () => {
      router.push('/discount');
    },
  },
  {
    iconname: 'amoeba',
    title: '订单管理',
    url: '/order',
    // show: true,
    router: () => {
      router.push('/order');
    },
  },
];

export const menu = menuConfig.map(m => {
  m.icon = menuIcons[`${m.iconname}`];
  m.activeIcon = menuIcons[`${m.iconname}_active`];
  return m;
});

export const MenuContext = React.createContext({
  update: () => {},
});
