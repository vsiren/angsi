import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message, Tooltip} from 'antd';
import {getCategory, productType, productStatus} from 'utils/const';
import moment from 'moment';
import router from 'umi/router';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getGoodsPage, insertProduct, modifyProduct, offShelfProduct, onShelfProduct, getConsoleCode } from '@/network/product';

const CreateDrawer = Form.create()((props) => {
  const onSubmit = values => {
    insertProduct(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="新增商品">
        <DrawerForm form={props.form} onSubmit={onSubmit} onClose={props.onClose} />
      </Drawer>
    )
  );
});
const EditDrawer = Form.create()((props) => {
  const onSubmit = values => {
    modifyProduct(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="编辑商品">
        <DrawerForm form={props.form} data={props.data} onSubmit={onSubmit} onClose={props.onClose} />
      </Drawer>
    )
   
  );
});

export default function () {
  const tableRef = useRef(null);
  const [createVisible, setCreateVisible] = useState(false);
  const [editVisible, setEditVisible] = useState(false);
  const [drawerInfo, setDrawerInfo] = useState({});
  const [providerMap, setProviderMap] = useState({});
  const [productCategoryMap, setProductCategoryMap] = useState({});
  // 上架
  const shelves = (record) => {
    // todo
    onShelfProduct({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  // 下架
  const deshelves = record => {
    // todo
    offShelfProduct({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  const toggleEditDrawer = (record) => {
    const status = !editVisible;
    setEditVisible(status);
    console.log('record', record);
    setDrawerInfo(record);
  };
  const gotoSpecifications = (record) => {
    router.push(`/specifications?id=${record.id}`);
  };
  useEffect(() => {
    getConsoleCode().then(res => {
      if (res.success) {
        
        setProviderMap(res.data.providerMap);
        setProductCategoryMap(res.data.productCategoryMap);
       
      } else {
        message.error(res.msg);
      }
    });
  },[])


  const columns = [
    {
      title: '秒杀活动ID',
      dataIndex: 'id',
      key: 'id',
    },
   
    {
      title: '秒杀活动名称',
      dataIndex: 'supplierName',
      key: 'supplierName',
    },
    {
      title: '商品ID',
      dataIndex: 'supplierId',
      key: 'supplierId',
    },
    {
      title: '商品名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '参与秒杀活动规格名称',
      dataIndex: 'abbr',
      key: 'abbr',
    },
    {
      title: '秒杀商品折扣（%）',
      dataIndex: 'image',
      key: 'image',
      render: text => {
        return text && <img src={text} className={style['img-mini']} />;
      },
    },
    {
      title: '秒杀商品份数/天',
      dataIndex: 'cornerMark',
      key: 'cornerMark',
    },
    {
      title: '会员专享',
      dataIndex: 'type',
      key: 'type',
      render: text => {
        return productType[text];
      },
    },
    {
      title: '限购',
      dataIndex: 'category',
      key: 'category',
      render: text => {
        return getCategory[text];
      },
    },
    {
      title: '秒杀活动开抢时间',
      dataIndex: 'detail',
      key: 'detail',
      render:text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '秒杀活动开始日期',
      dataIndex: 'detail',
      key: 'detail',
      render:text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '秒杀活动结束日期',
      dataIndex: 'detail',
      key: 'detail',
      render:text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '商品排序',
      dataIndex: 'seq',
      key: 'seq',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        return productStatus[text];
      },
    },
    {
      title: '操作人',
      dataIndex: 'modifier',
      key: 'modifier',
    },
    {
      title: '操作时间',
      dataIndex: 'gmtModified',
      key: 'gmtModified',
      render: text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      dataIndex: 'opt',
      key: 'opt',
      fixed: 'right',
      render: (text, record) => {
        return (
          <div>
            
            <a className={style['a-link']} onClick={() => toggleEditDrawer(record)}>编辑</a>
            
            <a className={style['a-link']} onClick={() => gotoSpecifications(record)}>查看规格</a>

            {
              record.status === 1 || record.status === 3
                ? (
                  <Popconfirm
                    title="确认上架?"
                    onConfirm={() => shelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>上架</a>

                  </Popconfirm>
                )
                :(
                  <Popconfirm
                    title="确认下架?"
                    onConfirm={() => deshelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>下架</a>

                  </Popconfirm>
                )
            }
          </div>
        );
      },
    },

  ];

  const toggleAddDrawer = () => {
    const status = !createVisible;
    setCreateVisible(status);
  };
  const onCreateSuccess = (res) => {
    if (res.success) {
      message.success('创建成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleAddDrawer();
    } else {
      message.error(res.msg);
    }
  };
  const onEditSuccess = (res) => {
    if (res.success) {
      message.success('修改成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleEditDrawer();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <Layout title="秒杀活动管理">
      <Button type="primary" className={style['btn-add']} onClick={toggleAddDrawer}>新增秒杀活动</Button>

      <div className={style['table-cont']}>
        <Table
          scroll={{x: 'max-content'}}
          ref={tableRef}
          initQuery
          columns={columns}
          queryData={({pagination, searchKey}) => {
            console.log('pagination:', pagination);
            console.log('searchKey', searchKey);

            return new Promise((resolve) => {
              getGoodsPage({
                ...pagination,
                ...searchKey,
              }).then(res => {
                if (res.success) {
                  resolve({
                    data: res.data,
                    total: res.total,
                  });
                }
              });
            });
          }}
        />
      </div>

      <CreateDrawer tableRef={tableRef} visible={createVisible} onClose={toggleAddDrawer} onSuccess={onCreateSuccess} />
      <EditDrawer tableRef={tableRef} visible={editVisible} onClose={toggleEditDrawer} data={drawerInfo} onSuccess={onEditSuccess} />


    </Layout>
  );
}
