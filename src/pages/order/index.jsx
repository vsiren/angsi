import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message, Tooltip, Input, DatePicker, Modal, Row, Select} from 'antd';
import {getCategory, productType, productStatus} from 'utils/const';
import moment from 'moment';
import router from 'umi/router';
import store from 'utils/store';
import {objToArr} from 'utils/util';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getOrderByPage, orderRefund, orderRetry } from '@/network/order';
import CustomSelect from '@/components/CustomSelect';
import XFormItem from '@/components/XFormItem';

const {RangePicker} = DatePicker;


export default function () {
  const tableRef = useRef(null);
  const [modalInfo, setModalInfo] = useState({});
  const [providerMap, setProviderMap] = useState({});
  const [productCategoryMap, setProductCategoryMap] = useState({});
  const [reason, setReason] = useState('');
  const [visible, setVisible] = useState(false);
  const [retryModalVisible, setRetryModalVisible] = useState(false);
  const code = store.getKey('code');
  const orderStatusMap = code.orderStatusMap;
  const orderTypeMap = code.orderTypeMap;
  const CustomSearchForm = Form.create()(({ values, form, onSearchSubmit }) => {
    const { getFieldDecorator } = form;

    let orderStatusArr = objToArr(orderStatusMap);
    orderStatusArr.push({
      label:'全部',
      value:null
    })
    const reset = () => {
      form.resetFields();
      // 重置并查询
      tableRef && tableRef.current.search();
    };
    return (
      <div style={{marginBottom: '20px'}}>
        <Form layout="inline">
          <div style={{marginBottom: 15, display:'flex'}}>
            <XFormItem label="订单号" width={260} style={{marginRight:'20px'}}>
              {getFieldDecorator('orderId', {
                initialValue: null,
                rules: [],
              })(<Input style={{width: '200px'}} />)}
            </XFormItem>
            <XFormItem label="用户ID" width={200} style={{marginRight:'20px'}}>
              {getFieldDecorator('userId', {
                initialValue: null,
                rules: [],
              })(<Input />)}
            </XFormItem>
            <XFormItem label="充值账号"  width={200} style={{marginRight:'20px'}}>
              {getFieldDecorator('rechargeAccount', {
                initialValue: null,
                rules: [],
              })(<Input />)}
            </XFormItem>
          </div>
          <div style={{display: 'flex'}}>
            <XFormItem label="订单状态" width={320} style={{marginRight:'20px'}}>
              {getFieldDecorator('status', {
                initialValue: null,
              })(
                <Select>
                  {
                    orderStatusArr.map(item => {
                      console.log('item', item);
                      
                      return (
                        <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                      );
                    })
                  }
                </Select>
                // <CustomSelect
                //   style={{width: '120px'}}
                //   renderOptions={(list) => {
                //     console.log('orderStatusArr', orderStatusArr);
                //     return orderStatusArr.map(item => {
                //       console.log('item', item);
                //       return (
                //         <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                //       );
                //     });
                //   }}
                // />,
              )}
            </XFormItem>
            <XFormItem label="充值时间" width={320} style={{marginRight:'20px'}}>
              {getFieldDecorator('date', {
                initialValue: null,
                rules: [],
              })(<RangePicker />)}
            </XFormItem>

            <Form.Item>
              <Button type="primary" onClick={onSearchSubmit}>
              查询
              </Button>
              <Button type="default" style={{marginLeft: '10px'}} onClick={reset}>
              重置
              </Button>
            </Form.Item>
          </div>


        </Form>

      </div>
    );
  });

  useEffect(() => {

  }, []);

  const gotoSuborder = (record) => {
    router.push(`/suborder?id=${record.id}&status=${record.status}&payAmount=${record.payAmount}&refundAmount=${record.refundAmount}&type=${record.type}&userId=${record.userId}&gmtCreated=${moment(record.gmtCreated).format('YYYY-MM-DD HH:mm:ss')}`);
  };
  const openRetryModal = (record) => {
    setRetryModalVisible(true);
    setModalInfo(record);
  };
  const showModal = (record) => {
    setVisible(true);
    setModalInfo(record);
  };

  // 发起退款
  const handleOk = e => {
    const hide = message.loading('正在申请退款', 0);
    orderRefund({
      orderId: modalInfo.id,
      remark: reason,
    }).then(res => {
      hide();
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
    setVisible(false);
  };

  const handleCancel = e => {
    console.log(e);
    setVisible(false);
  };
  const reasononChange = e => {
    setReason(e.target.value);
  };
  // 发起重试
  const handleOkRetry = () => {
    const hide = message.loading('正在申请重试', 0);

    orderRetry({
      orderId: modalInfo.id,
      remark: reason,
    }).then(res => {
      hide();
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
    setRetryModalVisible(false);
  };
  const handleCancelRetry = () => {
    setRetryModalVisible(false);
  };
  const columns = [
    {
      title: '订单号',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '用户ID',
      dataIndex: 'userId',
      key: 'userId',
    },
    {
      title: '购买方式',
      dataIndex: 'type',
      key: 'type',
      render: text => {
        return orderTypeMap[text]
      }
    },
    {
      title: '订单金额（元）',
      dataIndex: 'payAmount',
      key: 'payAmount',
    },
    {
      title: '订单创建时间',
      dataIndex: 'gmtCreated',
      key: 'gmtCreated',
      render: text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '订单状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        return orderStatusMap[text];
      },
    },
    {
      title: '操作',
      dataIndex: 'opt',
      key: 'opt',
      render: (text, record) => {
        return (
          <div>
            <a className={style['a-link']} onClick={() => gotoSuborder(record)}>查看</a>
            {
              record.status === 6 && <a className={style['a-link']} onClick={(e) => openRetryModal(record)}>重试</a>
            }
            {
              (record.status === 6 || record.status === 11) && <a className={style['a-link']} onClick={e => showModal(record)}>退款</a>
            }
          </div>
        );
      },
    },

  ];


  return (
    <Layout title="订单管理">


      <div className={style['table-cont']}>
        <Table
          CustomSearchForm={CustomSearchForm}
          scroll={{x: 'max-content'}}
          ref={tableRef}
          initQuery
          columns={columns}
          queryData={({pagination, searchKey}) => {
            console.log('pagination:', pagination);
            console.log('searchKey', searchKey);
            if (searchKey && searchKey.date) {
              searchKey.startDate = moment(searchKey.date[0]).format('YYYY-MM-DD');
              searchKey.endDate = moment(searchKey.date[1]).format('YYYY-MM-DD');
            }
            return new Promise((resolve) => {
              getOrderByPage({
                ...pagination,
                ...searchKey,
              }).then(res => {
                if (res.success) {
                  resolve({
                    data: res.data,
                    total: res.total,
                  });
                }
              });
            });
          }}
        />
      </div>
      <Modal
        title="退款"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="确定"
        cancelText="取消"
      >
        <h3>确定对订单ID：{modalInfo.id}发起退款？</h3>
        <h3>退款金额为￥{modalInfo.refundAmount}</h3>
        <Input placeholder="备注：退款详情（可为空）" onChange={reasononChange} />
      </Modal>

      <Modal
        title="重试"
        visible={retryModalVisible}
        onOk={handleOkRetry}
        onCancel={handleCancelRetry}
        okText="确定"
        cancelText="取消"
      >
        <React.Fragment>
          <h3>确定对订单ID：{modalInfo.id}发起充值重试？</h3>
          <Input placeholder="备注：重试详情（可为空）" onChange={reasononChange} />

        </React.Fragment>
      </Modal>


    </Layout>
  );
}
