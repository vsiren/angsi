import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message, Tooltip, Input, DatePicker} from 'antd';
import {getCategory, productType, productStatus} from 'utils/const';
import moment from 'moment';
import router from 'umi/router';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getGoodsPage, insertProduct, modifyProduct, offShelfProduct, onShelfProduct, getConsoleCode } from '@/network/product';


const {RangePicker} = DatePicker;

const CreateDrawer = Form.create()((props) => {
  const onSubmit = values => {
    insertProduct(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="新增商品">
        <DrawerForm form={props.form} onSubmit={onSubmit} onClose={props.onClose} />
      </Drawer>
    )
  );
});
const EditDrawer = Form.create()((props) => {
  const onSubmit = values => {
    modifyProduct(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="编辑商品">
        <DrawerForm form={props.form} data={props.data} onSubmit={onSubmit} onClose={props.onClose} />
      </Drawer>
    )
   
  );
});

export default function () {
  const tableRef = useRef(null);
  const [createVisible, setCreateVisible] = useState(false);
  const [editVisible, setEditVisible] = useState(false);
  const [drawerInfo, setDrawerInfo] = useState({});
  const [providerMap, setProviderMap] = useState({});
  const [productCategoryMap, setProductCategoryMap] = useState({});

  const CustomSearchForm = Form.create()(({ values, form, onSearchSubmit }) => {
    const { getFieldDecorator } = form;

    const reset = () => {
      form.resetFields();
      // 重置并查询
      tableRef && tableRef.current.search();
    };
    return (
      <div style={{marginBottom: '20px'}}>
        <Form layout="inline">
          <Form.Item label="用户昵称">
            {getFieldDecorator('name', {
              initialValue: '',
              rules: [],
            })(<Input />)}
          </Form.Item>
          <Form.Item label="注册时间">
            {getFieldDecorator('age', {
              initialValue: '',
              rules: [],
            })(<RangePicker />)}
          </Form.Item>
          <Form.Item>
            <Button type="primary" onClick={onSearchSubmit}>
              查询
            </Button>
            <Button type="default" style={{marginLeft: '10px'}} onClick={reset}>
              重置
            </Button>
          </Form.Item>
        </Form>

      </div>
    );
  });
  // 上架
  const shelves = (record) => {
    // todo
    onShelfProduct({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  // 下架
  const deshelves = record => {
    // todo
    offShelfProduct({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  const toggleEditDrawer = (record) => {
    const status = !editVisible;
    setEditVisible(status);
    console.log('record', record);
    setDrawerInfo(record);
  };
  const gotoSpecifications = (record) => {
    router.push(`/specifications?id=${record.id}`);
  };
  useEffect(() => {
    getConsoleCode().then(res => {
      if (res.success) {
        
        setProviderMap(res.data.providerMap);
        setProductCategoryMap(res.data.productCategoryMap);
       
      } else {
        message.error(res.msg);
      }
    });
  },[])


  const columns = [
    {
      title: '用户ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'openid',
      dataIndex: 'supplierId',
      key: 'supplierId',
    },
    {
      title: '用户昵称',
      dataIndex: 'supplierName',
      key: 'supplierName',
    },
    {
      title: '用户头像',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '是否是会员',
      dataIndex: 'abbr',
      key: 'abbr',
    },
    {
      title: '会员等级',
      dataIndex: 'image',
      key: 'image',
      render: text => {
        return text && <img src={text} className={style['img-mini']} />;
      },
    },
    {
      title: '会员到期日',
      dataIndex: 'cornerMark',
      key: 'cornerMark',
    },
  
    {
      title: '注册时间',
      dataIndex: 'gmtModified',
      key: 'gmtModified',
      render: text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },

  ];

  const toggleAddDrawer = () => {
    const status = !createVisible;
    setCreateVisible(status);
  };
  const onCreateSuccess = (res) => {
    if (res.success) {
      message.success('创建成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleAddDrawer();
    } else {
      message.error(res.msg);
    }
  };
  const onEditSuccess = (res) => {
    if (res.success) {
      message.success('修改成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleEditDrawer();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <Layout title="用户管理">


      <div className={style['table-cont']}>
        <Table
          CustomSearchForm={CustomSearchForm}
          scroll={{x: 'max-content'}}
          ref={tableRef}
          initQuery
          columns={columns}
          queryData={({pagination, searchKey}) => {
            console.log('pagination:', pagination);
            console.log('searchKey', searchKey);

            return new Promise((resolve) => {
              getGoodsPage({
                ...pagination,
                ...searchKey,
              }).then(res => {
                if (res.success) {
                  resolve({
                    data: res.data,
                    total: res.total,
                  });
                }
              });
            });
          }}
        />
      </div>

      <CreateDrawer tableRef={tableRef} visible={createVisible} onClose={toggleAddDrawer} onSuccess={onCreateSuccess} />
      <EditDrawer tableRef={tableRef} visible={editVisible} onClose={toggleEditDrawer} data={drawerInfo} onSuccess={onEditSuccess} />


    </Layout>
  );
}
