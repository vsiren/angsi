import React, {Fragment, Component} from 'react';
import {Form, Input, Select, Button, Modal, Drawer, Radio} from 'antd';
import Layout from '@/components/Layout';
import XFormItem from '@/components/XFormItem';
import CustomSelect from '@/components/CustomSelect';
import style from './index.less';
import Upload from '@/components/Upload';

class DrawerForm extends Component {
  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        console.log('err,', err);
      }
      // todo
      console.log('values', values);
      if (this.props.data) {
        values.id = this.props.data.id;
      }
      values.productId = this.props.productId;

      this.props.onSubmit(values);
    });
  };

  render() {
    const {getFieldDecorator} = this.props.form;
    const data = this.props.data || {};
    return (
      <Form onSubmit={this.onSubmit}>
        <XFormItem labelWidth={90} width={400} label="规格sku">
          {getFieldDecorator('sku', {
            initialValue: data.sku,
            rules: [
              {
                required: true,
                message: '请输入规格sku',
              },
            ],
          })(<Input />)}
        </XFormItem>
        <XFormItem labelWidth={120} width={400} label="买会员弹窗(需前往折扣绑定)">
          {getFieldDecorator('membershipPopup', {
            initialValue: data.membershipPopup,
            rules: [
              {
                required: true,
                message: '请选择买会员弹窗',
              },
            ],
          })(
              <Radio.Group>
                <Radio value="Y">有</Radio>
                <Radio value="N">无</Radio>
              </Radio.Group>

          )}
        </XFormItem>

        <XFormItem labelWidth={120} width={400} label="规格名称">
          {getFieldDecorator('name', {
            initialValue: data.name,

            rules: [
              {
                required: true,
                message: '请输入规格名称',
              },
            ],
          })(
            <Input placeholder="请输入规格名称" />,
          )}
        </XFormItem>

        <XFormItem labelWidth={120} width={400} label="原价">
          {getFieldDecorator('price', {
            initialValue: data.price,

            rules: [
              {
                required: true,
                message: '请输入原价',
              },
            ],
          })(
            <Input placeholder="请输入原价" />,
          )}
        </XFormItem>

        <XFormItem labelWidth={120} width={400} label="角标文案">
          {getFieldDecorator('cornerMark', {
            initialValue: data.cornerMark,

            rules: [
              {
                required: false,
                message: '请输入角标文案',
              },
            ],
          })(
            <Input placeholder="请输入角标文案" />,
          )}
        </XFormItem>

        <XFormItem labelWidth={120} width={400} label="规格成本价">
          {getFieldDecorator('costPrice', {
            initialValue: data.costPrice,

            rules: [
              {
                required: true,
                message: '请输入规格成本价',
              },
            ],
          })(
            <Input placeholder="请输入规格成本价" />,
          )}
        </XFormItem>
        <XFormItem labelWidth={120} width={400} label="规格库存">
          {getFieldDecorator('stock', {
            initialValue: data.stock,

            rules: [
              {
                required: true,
                message: '请输入规格库存',
              },
            ],
          })(
            <Input placeholder="请输入规格库存" />,
          )}
        </XFormItem>
        <XFormItem labelWidth={120} width={400} label="规格最大购买数量">
          {getFieldDecorator('maxPurchaseCount', {
            initialValue: data.maxPurchaseCount,

            rules: [
              {
                required: true,
                message: '请输入规格最大购买数量',
              },
            ],
          })(
            <Input placeholder="请输入规格最大购买数量" />,
          )}
        </XFormItem>
        <XFormItem labelWidth={120} width={400} label="规格排序">
          {getFieldDecorator('seq', {
            initialValue: data.seq,

            rules: [
              {
                required: true,
                message: '请输入规格排序',
              },
            ],
          })(
            <Input placeholder="请输入规格排序" />,
          )}
        </XFormItem>


        <div className={style['button-group']}>
          <Button htmlType="submit" type="primary" className={style['btn-save']} disabled={data.status === 2}>保存</Button>
          <Button onClick={this.props.onClose}>取消</Button>
        </div>


      </Form>
    );
  }
}
export default DrawerForm;
