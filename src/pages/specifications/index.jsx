import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message} from 'antd';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getProductItemPage, insertProductItem, modifyProductItem, offShelfProductItem, onShelfProductItem} from '@/network/productItem';
import XBack from '@/components/XBack';
import {getCategory, productType, productStatus} from 'utils/const';
import moment from 'moment';
import { RightCircleFilled } from '@ant-design/icons';

const CreateDrawer = Form.create()((props) => {
  const onSubmit = values => {
    insertProductItem(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="新增规格">
        <DrawerForm form={props.form} onSubmit={onSubmit} onClose={props.onClose} productId={props.productId} />
      </Drawer>
    )
  );
});
const EditDrawer = Form.create()((props) => {
  const onSubmit = values => {
    modifyProductItem(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="编辑规格">
        <DrawerForm form={props.form} onSubmit={onSubmit} data={props.data} onClose={props.onClose} productId={props.productId} />
      </Drawer>
    )
  );
});

export default function (props) {
  const tableRef = useRef(null);
  const [createVisible, setCreateVisible] = useState(false);
  const [editVisible, setEditVisible] = useState(false);
  const [drawerInfo, setDrawerInfo] = useState({});
  // 上架
  const shelves = (record) => {
    // todo
    onShelfProductItem({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  // 下架
  const deshelves = record => {
    // todo
    offShelfProductItem({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  const toggleEditDrawer = (record) => {
    const status = !editVisible;
    setEditVisible(status);
    setDrawerInfo(record);
  };


  const columns = [
    {
      title: '商品ID',
      dataIndex: 'productId',
      key: 'productId',
    },
    {
      title: '商品规格SKU',
      dataIndex: 'sku',
      key: 'sku',
    },
    {
      title: '商品规格名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '原价',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: '规格角标文案',
      dataIndex: 'cornerMark',
      key: 'cornerMark',
    },
    {
      title: '买会员弹窗',
      dataIndex: 'membershipPopup',
      key: 'membershipPopup',
      render: text => {
        return text === 'Y' ? '是' : '否'
      }
    },
    {
      title: '规格成本价',
      dataIndex: 'costPrice',
      key: 'costPrice',
    },
    {
      title: '规格库存',
      dataIndex: 'stock',
      key: 'stock',
    },
    {
      title: '规格最大购买数量',
      dataIndex: 'maxPurchaseCount',
      key: 'maxPurchaseCount',
    },
    {
      title: '规格排序',
      dataIndex: 'seq',
      key: 'seq',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: text => {
        return productStatus[text];
      },
    },
    {
      title: '操作人',
      dataIndex: 'modifier',
      key: 'modifier',
    },
    {
      title: '操作时间',
      dataIndex: 'gmtModified',
      key: 'gmtModified',
      render: text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      dataIndex: 'opt',
      key: 'opt',
      fixed: 'right',
      render: (text, record) => {
        return (
          <div>
            <a className={style['a-link']} onClick={() => toggleEditDrawer(record)}>编辑</a>
            {
              record.status === 1 || record.status === 3
                ? (
                  <Popconfirm
                    title="确认上架?"
                    onConfirm={() => shelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>上架</a>

                  </Popconfirm>
                )
                : (
                  <Popconfirm
                    title="确认下架?"
                    onConfirm={() => deshelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>下架</a>

                  </Popconfirm>
                )
            }
          </div>
        );
      },
    },

  ];

  const toggleAddDrawer = () => {
    const status = !createVisible;
    setCreateVisible(status);
  };
  const onCreateSuccess = (res) => {
    if (res.success) {
      message.success('创建成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleAddDrawer();
    } else {
      message.error(res.msg);
    }
  };
  const onEditSuccess = (res) => {
    if (res.success) {
      message.success('修改成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleEditDrawer();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <Layout title="规格管理">
      <div className={style.back}>
        <XBack />

      </div>
      <Button type="primary" className={style['btn-add']} onClick={toggleAddDrawer}>新增规格</Button>

      <Table
        scroll={{x: 'max-content'}}
        ref={tableRef}
        initQuery
        columns={columns}
        queryData={({pagination, searchKey}) => {
          console.log('pagination:', pagination);
          console.log('searchKey', searchKey);
          console.log('props', props);
          return new Promise((resolve) => {
            getProductItemPage({
              productId: props.location.query.id,
              ...pagination,
              ...searchKey,
            }).then(res => {
              if (res.success) {
                resolve({
                  data: res.data,
                  total: res.total,
                });
              }
            });
          });
        }}
      />
      <CreateDrawer visible={createVisible} onClose={toggleAddDrawer} onSuccess={onCreateSuccess} productId={props.location.query.id} />
      <EditDrawer visible={editVisible} onClose={toggleEditDrawer} data={drawerInfo} onSuccess={onEditSuccess} productId={props.location.query.id} />


    </Layout>
  );
}
