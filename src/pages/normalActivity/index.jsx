import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message} from 'antd';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getEventPage, insertEvent, modifyEvent, offShelfEvent, onShelfEvent} from '@/network/event';
import moment from 'moment'
import store from 'utils/store';

const CreateDrawer = Form.create()((props) => {
  const onSubmit = values => {
    insertEvent(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="新增常规活动">
        <DrawerForm form={props.form} onSubmit={onSubmit} onClose={props.onClose} />
      </Drawer>
    )
   
  );
});
const EditDrawer = Form.create()((props) => {
  const onSubmit = values => {
    modifyEvent(values).then(res => {
      props.onSuccess(res);
    });
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="编辑常规活动">
        <DrawerForm form={props.form} onSubmit={onSubmit} data={props.data} onClose={props.onClose} />
      </Drawer>
    )
  
  );
});

export default function () {
  const tableRef = useRef(null);
  const [createVisible, setCreateVisible] = useState(false);
  const [editVisible, setEditVisible] = useState(false);
  const [drawerInfo, setDrawerInfo] = useState({});
  const code = store.getKey('code');
  const productTypeMap = code.productTypeMap;
  // 上架
  const shelves = (record) => {
    // todo
    onShelfEvent({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  // 下架
  const deshelves = record => {
    // todo
    offShelfEvent({
      id: record.id,
    }).then(res => {
      if (res.success) {
        message.success('操作成功');
        tableRef.current.search();
      } else {
        message.error(res.msg);
      }
    });
  };
  const toggleEditDrawer = (record) => {
    const status = !editVisible;
    setEditVisible(status);
    setDrawerInfo(record);
  };


  const columns = [
    {
      title: '常规活动ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '活动名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '有效期',
      dataIndex: 'termValidity',
      key: 'termValidity',
    },
    {
      title: '活动价折扣配置',
      dataIndex: 'eventRuleConsoleDTOList',
      key: 'eventRuleConsoleDTOList',
      render: (text, record) => {
        
        return (
          <div>
            {
              (text || []).map(item => <div>第{item.min}-第{item.max}名:{parseFloat(item.discount).toFixed(2)}%</div>)
            }
            <div>每天{record.countRefreshTime}刷新排名</div>
          </div>
        );
      },
    },
    {
      title: '商品ID',
      dataIndex: 'productId',
      key: 'productId',
    },
    {
      title: '商品名称',
      dataIndex: 'productName',
      key: 'productName',
    },
    {
      title: '商品类型',
      dataIndex: 'type',
      key: 'type',
      render: text => {
        return productTypeMap[text];
      },
    },
    {
      title: '是否限购',
      dataIndex: 'limitBuy',
      key: 'limitBuy',
      render: (text) => {
        return text === 'Y' ? '是' : '否';
      },
    },

    {
      title: '操作人',
      dataIndex: 'modifier',
      key: 'modifier',
    },
    {
      title: '操作时间',
      dataIndex: 'gmtModified',
      key: 'gmtModified',
      render: text => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      dataIndex: 'modifier',
      key: 'modifier',
      fixed: 'right',
      render: (text, record) => {
        return (
          <div>
            <a className={style['a-link']} onClick={() => toggleEditDrawer(record)}>编辑</a>
            {
              record.status === 2 || record.status === null
                ? (
                  <Popconfirm
                    title="确认上架?"
                    onConfirm={() => shelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>上架</a>

                  </Popconfirm>
                )
                : (
                  <Popconfirm
                    title="确认下架?"
                    onConfirm={() => deshelves(record)}
                    okText="确定"
                    cancelText="取消"
                  >
                    <a className={style['a-link']}>下架</a>

                  </Popconfirm>
                )
            }
          </div>
        );
      },
    },

  ];

  const toggleAddDrawer = () => {
    const status = !createVisible;
    setCreateVisible(status);
  };
  const onCreateSuccess = (res) => {
    if (res.success) {
      message.success('创建成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleAddDrawer();
    } else {
      message.error(res.msg);
    }
  };
  const onEditSuccess = (res) => {
    if (res.success) {
      message.success('修改成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleEditDrawer();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <Layout title="活动管理">
      <Button type="primary" className={style['btn-add']} onClick={toggleAddDrawer}>新增常规活动</Button>

      <Table
        scroll={{x: 'max-content'}}
        ref={tableRef}
        initQuery
        columns={columns}
        queryData={({pagination, searchKey}) => {
          console.log('pagination:', pagination);
          console.log('searchKey', searchKey);

          return new Promise((resolve) => {
            getEventPage({
              ...pagination,
              ...searchKey,
            }).then(res => {
              if (res.success) {
                (res.data || []).forEach(item => {
                  (item.eventRuleConsoleDTOList || []).forEach(it => {
                    it.discount =  (it.discount * 100).toFixed(2);
                  })
                });
                resolve({
                  data: res.data,
                  total: res.total,
                });
              }
            });
          });
        }}
      />
      <CreateDrawer visible={createVisible} onClose={toggleAddDrawer} onSuccess={onCreateSuccess} />
      <EditDrawer visible={editVisible} onClose={toggleEditDrawer} data={drawerInfo} onSuccess={onEditSuccess} />


    </Layout>
  );
}
