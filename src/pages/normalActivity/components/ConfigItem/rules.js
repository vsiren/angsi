
export const freeRules = [
  {
    field: 'cost',
    fieldName: '消耗',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
      // {
      // 	"operate":"near",
      // 	"operateName":"临近预算",
      // 	"values": [0.5,0.8,1],
      // 	"unit":"multiple"
      // }
    ],
  }, {
    field: 'view_count',
    fieldName: '曝光量',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'valid_click_count',
    fieldName: '点击量',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'cpc',
    fieldName: '平均点击均价',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'convert',
    fieldName: '转化量',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'convert_cost',
    fieldName: '转化成本',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
      // {
      // 	"operate":"gtBid",
      // 	"operateName":"高于出价",
      // 	"values":[],
      // 	"unit":"multiple"
      // },
      // {
      // 	"operate":"ltBid",
      // 	"operateName":"低于出价",
      // 	"values":[],
      // 	"unit":"multiple"
      // }
    ],
  }, {
    field: 'ctr',
    fieldName: '点击率',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'percentRange',
      },
    ],
  },
];

export const qualityRules = [
  {
    field: 'cost',
    fieldName: '消耗',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
      // {
      // 	"operate":"near",
      // 	"operateName":"临近预算",
      // 	"values": [0.5,0.8,1],
      // 	"unit":"multiple"
      // }
    ],
  }, {
    field: 'view_count',
    fieldName: '曝光量',
    operates: [
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'valid_click_count',
    fieldName: '点击量',
    operates: [
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'cpc',
    fieldName: '平均点击均价',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'convert',
    fieldName: '转化量',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'absolute',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'range',
      },
    ],
  }, {
    field: 'convert_cost',
    fieldName: '转化成本',
    operates: [
      {
        operate: 'gte',
        operateName: '大于等于',
        values: [],
        unit: 'absolute',
      },
      // {
      // 	"operate":"gtBid",
      // 	"operateName":"高于出价",
      // 	"values":[1.2,1.5],
      // 	"unit":"multiple"
      // }
    ],
  }, {
    field: 'ctr',
    fieldName: '点击率',
    operates: [
      {
        operate: 'lte',
        operateName: '小于等于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'range',
        operateName: '介于',
        values: [],
        unit: 'percentRange',
      },
    ],
  },
];
export const differentRules = [
  {
    field: 'cost',
    fieldName: '消耗',
    operates: [
      {
        operate: 'riseGt',
        operateName: '涨幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'riseLt',
        operateName: '涨幅低于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropGt',
        operateName: '跌幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropLt',
        operateName: '跌幅低于',
        values: [],
        unit: 'percent',
      },
    ],
  }, {
    field: 'view_count',
    fieldName: '曝光量',
    operates: [
      {
        operate: 'riseGt',
        operateName: '涨幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'riseLt',
        operateName: '涨幅低于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropGt',
        operateName: '跌幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropLt',
        operateName: '跌幅低于',
        values: [],
        unit: 'percent',
      },
    ],
  }, {
    field: 'convert',
    fieldName: '转化量',
    operates: [
      {
        operate: 'riseGt',
        operateName: '涨幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'riseLt',
        operateName: '涨幅低于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropGt',
        operateName: '跌幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropLt',
        operateName: '跌幅低于',
        values: [],
        unit: 'percent',
      },
    ],
  }, {
    field: 'convert_cost',
    fieldName: '转化成本',
    operates: [
      {
        operate: 'riseGt',
        operateName: '涨幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'riseLt',
        operateName: '涨幅低于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropGt',
        operateName: '跌幅高于',
        values: [],
        unit: 'percent',
      },
      {
        operate: 'dropLt',
        operateName: '跌幅低于',
        values: [],
        unit: 'percent',
      },
    ],
  },
];
