import React, { useState, useEffect, forwardRef } from 'react';
import { Form, Select } from 'antd';
import style from './index.less';
import XIcon from '@/components/XIcon';
import { freeRules, qualityRules, differentRules } from './rules';
import ConditionValueInput from '../ConditionValueInput';

const { Option } = Select;
const ConfigItem = (props) => {
  const [rulesConfig, setRulesConfig] = useState([]);
  const { getFieldDecorator } = props.form;
  const [operates, setOperates] = useState([]);
  const [unit, setUnit] = useState('absolute');
  const [conditions, setConditions] = useState({});
  const [selectedField, setSelectedField] = useState({});


  const inputonChange = value => {
    setConditions({
      conditionValue: value,
      ...conditions,
    });
    props.onChange && props.onChange(conditions);
  };
  
  console.log('initialValue', props.initialValue, props.customKey);
  return (
    <div className={style['rule-item']}>
      <div className={style['rule-row']}>
        <Form.Item>
          {getFieldDecorator(`conditionValue[${props.customKey}]`, {
            initialValue: props.initialValue || {},
            rules: [{
              required: true,
            }],
          })(
            <ConditionValueInput unit={unit} operates={operates} onChange={inputonChange} productItemList={props.productItemList} />,
          )}
        </Form.Item>
        <span className={style.bin} onClick={props.delete}>
          <XIcon type="iconlajitong" />
        </span>


      </div>
    </div>
  );
};

export default ConfigItem;
