import React, { useState, useEffect, useRef, forwardRef} from 'react';
import ConfigItem from '../ConfigItem';
import style from './index.less';

let id = 0;

const ConfigList = (props) => {
  const {getFieldDecorator, getFieldValue} = props.form;
  const remove = k => {
    const { form } = props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    // We need at least one passenger
    if (keys.length === 1) {
      return;
    }

    // can use data-binding to set
    form.setFieldsValue({
      keys: keys.filter(key => key !== k),
    });
  };
  const add = () => {
    const { form } = props;
    // can use data-binding to get
    const keys = form.getFieldValue('keys');
    const nextKeys = keys.concat(id++);
    // can use data-binding to set
    // important! notify form to detect changes
    form.setFieldsValue({
      keys: nextKeys,
    });
  };
  useEffect(() => {
    id = props.value.length;
  }, []);
  /*
  props.value
  [{
    min:xx,
    max:xx,
    discount:xx
  }]
  */
  const initialValue = props.value.map((item, index) => index);
  getFieldDecorator('keys', { initialValue });
  const keys = getFieldValue('keys');

  console.log('keys', keys);


  return (
    <div className={style['config-rules']}>
      <div className={style['rules-list']}>
        {
          keys.map(k => (
            <ConfigItem key={k} customKey={k} delete={() => { remove(k); }} type={props.type} form={props.form} initialValue={props.value[k]} productItemList={props.productItemList} />
          ))
        }
      </div>
      <div className={style.add} onClick={add}>
        <div>
          <span className={style['add-icon']}>+</span><span className={style['add-txt']}>添加条件</span>
        </div>

      </div>
    </div>
  );
};
export default ConfigList;
