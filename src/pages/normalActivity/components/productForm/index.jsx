import React, {Fragment, Component} from 'react';
import {Form, Input, Select, Button, Modal, Drawer, Radio, DatePicker, TimePicker} from 'antd';
import moment from 'moment';
import {objToArr} from 'utils/util';
import Layout from '@/components/Layout';
import XFormItem from '@/components/XFormItem';
import CustomSelect from '@/components/CustomSelect';
import style from './index.less';
import ConfigList from '../ConfigList';
import {getProductAll, getProductItem} from '@/network/product';

const { RangePicker } = DatePicker;

class DrawerForm extends Component {
  state = {
    productItemList: [],
  };

  onSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        console.log('err,', err);
      }
      // todo
      console.log('values', values);
      values.countRefreshTime = moment(values.countRefreshTime).format('HH:mm:ss');

      const { keys, conditionValue } = values;
      console.log('conditionValue', conditionValue);
      console.log('Merged values:', keys.map(key => conditionValue[key]));
      
      values.eventRuleConsoleDTOList = keys.map(key => {
        conditionValue[key].eventRuleProductItemConsoleDTOList.forEach(item => {
          item.discount = parseFloat(item.discount / 100).toFixed(2);
        });
        return {
          eventRuleId: conditionValue[key].eventRuleId,
          min: parseFloat(conditionValue[key].min),
          max: parseFloat(conditionValue[key].max),
          eventRuleProductItemConsoleDTOList: conditionValue[key].eventRuleProductItemConsoleDTOList,
        };
      });
      if (!values.isLongTerm) {
        values.startDate = moment(values.date[0]).format('YYYY-MM-DD');
        values.endDate = moment(values.date[1]).format('YYYY-MM-DD');
      }
      if (this.props.data) {
        values.id = this.props.data.id;
      }
      this.props.onSubmit(values);
    });
  };

  productonChange = value => {
    console.log('value', value);
    getProductItem({
      id: value,
    }).then(res => {
      if(res.success){
        this.setState({
          productItemList: res.data
        })
      }
    });
  };

  componentDidMount() {
    getProductAll({}).then(res => {
      if (res.success) {
        res.data = objToArr(res.data);
        this.setState({
          productList: res.data,
        });
      }
    });
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const data = this.props.data || {};
    console.log('isLong', this.productonChange);
    const format = 'HH:mm';
    const startDate = data.termValidity ? moment(data.termValidity.substr(0, 19)) : moment();
    const endDate = data.termValidity ? moment(data.termValidity.substr(20)) : moment();
    const termValidity = [startDate, endDate];
    return (
      <Form onSubmit={this.onSubmit}>
        <XFormItem labelWidth={90} width={400} label="活动名称">
          {getFieldDecorator('name', {
            initialValue: data.name,
            rules: [
              {
                required: true,
                message: '请输入活动名称',
              },
            ],
          })(
            <Input />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="折扣刷新时间">
          {getFieldDecorator('countRefreshTime', {
            initialValue: data.countRefreshTime ? moment(data.countRefreshTime, format) : moment(),
            rules: [
              {
                required: true,
                message: '请输入折扣刷新时间',
              },
            ],
          })(
            <TimePicker format={format} />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="所属商品">
          {getFieldDecorator('productId', {
            initialValue: data.productId,
            rules: [
              {
                required: true,
                message: '请选择所属商品名称',
              },
            ],
          })(
        
            <CustomSelect
              onChange={this.productonChange}
              queryData={() => {
                return new Promise((resolve, reject) => {
                  getProductAll({}).then(res => {
                    if (res.success) {
                      res.data = objToArr(res.data);
                      resolve(res.data);
                    }
                  });
                });
              }}
              renderOptions={(list) => {
                return list.map(item => {
                  return (
                    <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                  );
                });
              }}
            />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="名次折扣配置" style={{borderWidth: 0}}>
          {getFieldDecorator('eventRuleConsoleDTOList', {
            initialValue: data.eventRuleConsoleDTOList || [],
            rules: [
              {
                required: false,
                message: '请输入活动折扣配置',
              },
            ],
          })(
            <ConfigList form={this.props.form} productItemList={this.state.productItemList} />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="活动有效期">
          {getFieldDecorator('isLongTerm', {
            initialValue: data.isLongTerm,
            rules: [
              {
                required: true,
                message: '请输入活动有效期',
              },
            ],
          })(
            <Radio.Group>
              <Radio value>长期有效</Radio>
              <Radio value={false}>设置起止时间</Radio>
            </Radio.Group>,
          )}
        </XFormItem>
        {
          this.props.form.getFieldValue('isLongTerm') === false
           && (
           <XFormItem labelWidth={90} width={400} label="起止时间">
             {getFieldDecorator('date', {
               initialValue: termValidity,
               rules: [
                 {
                   required: true,
                   message: '请输入起止时间',
                 },
               ],
             })(
               <RangePicker />,
             )}
           </XFormItem>
           )
        }
        <XFormItem labelWidth={90} width={400} label="是否限购">
          {getFieldDecorator('limitBuy', {
            initialValue: data.limitBuy,
            rules: [
              {
                required: true,
                message: '请选择是否限购',
              },
            ],
          })(
            <Radio.Group>
              <Radio value="Y">是</Radio>
              <Radio value="N">否</Radio>
            </Radio.Group>,
          )}
        </XFormItem>


        <div className={style['button-group']}>
          <Button htmlType="submit" type="primary" className={style['btn-save']} disabled={data.status === 1}>保存</Button>
          <Button onClick={this.props.onClose}>取消</Button>
        </div>


      </Form>
    );
  }
}
export default DrawerForm;
