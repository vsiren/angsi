import React, { useState } from 'react';
import { Input } from 'antd';
import style from './index.less';

class ConditionValueInput extends React.Component {
  constructor(props) {
    super(props);
    let eventRuleProductItemConsoleDTOList = this.props.value.eventRuleProductItemConsoleDTOList;
    if (!eventRuleProductItemConsoleDTOList) {
      eventRuleProductItemConsoleDTOList = this.props.productItemList.map((item, index) => {
        return {
          discount: 0,
          productItemId: item.id,
          productItemName: item.name,
        };
      });
    }
    this.state = {
      value: {
        ...this.props.value,
        eventRuleProductItemConsoleDTOList,
      },
    };
  }


  componentDidMount() {

  }

  componentDidUpdate(prevProps) {
    if (prevProps.productItemList !== this.props.productItemList) {
      console.log('this.props.productItemList', this.props.productItemList);
      const eventRuleProductItemConsoleDTOList = this.props.productItemList.map((item, index) => {
        return {
          discount: 0,
          productItemId: item.id,
          productItemName: item.name,
        };
      });
      this.setState({
        value: {
          ...this.state.value,
          eventRuleProductItemConsoleDTOList,
        },
      },() => {
        this.props.onChange && this.props.onChange(this.state.value);
      });
    }
  }

  inputChange = (e, position) => {
    this.setState({
      value: {
        ...this.state.value,
        [position]: e.target.value,
      },
    }, () => {
      this.props.onChange && this.props.onChange(this.state.value);
    });
  };

  inputDiscountChange = (e, index) => {
    const eventRuleProductItemConsoleDTOList = this.state.value.eventRuleProductItemConsoleDTOList;
    eventRuleProductItemConsoleDTOList[index].discount = e.target.value;
    this.setState({
      value: {
        ...this.state.value,
        eventRuleProductItemConsoleDTOList,
      },
    },() => {
      this.props.onChange && this.props.onChange(this.state.value);

    });
  };

  render() {
    console.log('conditionValueInput', this.state.value);

    return (
      <div className={style['range-input-cont']}>
        <Input className={style['range-input']} value={this.props.value.min} onChange={e => this.inputChange(e, 'min')} />
        <span className={style.line}>-</span>
        <Input className={style['range-input']} value={this.props.value.max} onChange={e => this.inputChange(e, 'max')} />
        <span className={style.mlr10}>名</span>
        <div>
          {
            (this.state.value.eventRuleProductItemConsoleDTOList || []).map((item, index) => {
              return (
                <div className={style['product-item']}>
                  <div className={style['productItem-name']}>{item.productItemName}</div>
                  <Input className={style['range-input']} value={item.discount} onChange={e => this.inputDiscountChange(e, index)} />
                  <span className={style.mlr10}>%</span>
                </div>
              );
            })
          }
        </div>


      </div>
    );
  }
}
export default ConditionValueInput;
