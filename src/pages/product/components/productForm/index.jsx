import React, {Fragment, Component} from 'react';
import {Form, Input, Select, Button, Modal, Drawer, Radio, message} from 'antd';
import {categoryList} from 'utils/const';
import XFormItem from '@/components/XFormItem';
import CustomSelect from '@/components/CustomSelect';
import style from './index.less';
import Upload from '@/components/Upload';
import Editor from '@/components/WangEditor';
import {getConsoleCode} from '@/network/product';
import store from 'utils/store';

class DrawerForm extends Component {
  state = {
    providerArr: [],
    productCategoryArr: [],
    productTypeArr:[],
    payMerchantArr:[]
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        console.log('err,', err);
      }
      // todo
      console.log('values', values);
      values.supplierName = this.state.providerArr.find(item => item.value === values.supplierId).label;
      if (this.props.data) {
        values.id = this.props.data.id;
      }
      this.props.onSubmit(values);
    });
  };

  objToArr = obj => {
    const arr = [];
    for (const i in obj) {
      arr.push({
        value: parseInt(i),
        label: obj[i],
      });
    }
    return arr;
  };

  componentDidMount() {
    const code = store.getKey('code');
    const providerMap = code.providerMap;
    const productCategoryMap = code.productCategoryMap;
    const productTypeMap = code.productTypeMap;
    const payMerchantMap = code.payMerchantMap;
    const providerArr = this.objToArr(providerMap);
    const productCategoryArr = this.objToArr(productCategoryMap);
    const productTypeArr = this.objToArr(productTypeMap);
    const payMerchantArr = this.objToArr(payMerchantMap);
    console.log('providerArr', providerArr, productTypeArr);
    this.setState({
      providerArr,
      productCategoryArr,
      productTypeArr,
      payMerchantArr
    });
    
  }

  render() {
    const {getFieldDecorator} = this.props.form;
    const data = this.props.data || {};
    console.log('type', data.type, typeof (data.type), data);
    return (
      <Form onSubmit={this.onSubmit}>
        <XFormItem labelWidth={90} width={400} label="供应商名称">
          {getFieldDecorator('supplierId', {
            initialValue: data.supplierId,
            rules: [
              {
                required: true,
                message: '请输入供应商名称',
              },
            ],
          })(<CustomSelect
            
            renderOptions={(list) => {
              return this.state.providerArr.map(item => {
                return (
                  <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                );
              });
            }}
          />)}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="商品类型" style={{borderWidth: 0}}>
          {getFieldDecorator('type', {
            initialValue: data.type,
            rules: [
              {
                required: true,
                message: '请输入商品类型',
              },
            ],
          })(
            <Radio.Group>
              {
                this.state.productTypeArr.map(item => <Radio value={item.value}>{item.label}</Radio>)
              }
            </Radio.Group>,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="商品分类">
          {getFieldDecorator('category', {
            initialValue: data.category,
            rules: [
              {
                required: true,
                message: '请输入商品分类',
              },
            ],
          })(
            <CustomSelect
             
              renderOptions={() => {
                return this.state.productCategoryArr.map(item => {
                  return (
                    <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                  );
                });
              }}
            />,
          )}
        </XFormItem>

        <XFormItem labelWidth={90} width={400} label="支付商户">
          {getFieldDecorator('wechatMerchant', {
            initialValue: data.wechatMerchant,
            rules: [
              {
                required: true,
                message: '请输入支付商户',
              },
            ],
          })(
            <CustomSelect
             
              renderOptions={() => {
                return this.state.payMerchantArr.map(item => {
                  return (
                    <Select.Option value={item.value} key={item.value}>{item.label}</Select.Option>
                  );
                });
              }}
            />,
          )}
        </XFormItem>

        <XFormItem labelWidth={90} width={400} label="商品名称">
          {getFieldDecorator('name', {
            initialValue: data.name,

            rules: [
              {
                required: true,
                message: '请输入商品名称',
              },
            ],
          })(
            <Input placeholder="请输入商品名称" />,
          )}
        </XFormItem>

        <XFormItem labelWidth={90} width={400} label="商品名简称">

          {getFieldDecorator('abbr', {
            initialValue: data.abbr,

            rules: [
              {
                required: false,
                message: '请输入商品名简称',
              },
            ],
          })(
            <Input placeholder="请输入商品名简称" />,
          )}
        </XFormItem>

        <XFormItem labelWidth={90} width={400} label="商品角标">
          {getFieldDecorator('cornerMark', {
            initialValue: data.cornerMark,

            rules: [
              {
                required: false,
                message: '请输入商品角标',
              },
            ],
          })(
            <Input placeholder="请输入商品角标" />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="商品排序">
          {getFieldDecorator('seq', {
            initialValue: data.seq,

            rules: [
              {
                required: true,
                message: '请输入商品排序',
              },
            ],
          })(
            <Input placeholder="请输入商品排序" />,
          )}
        </XFormItem>


        <XFormItem labelWidth={90} width={400} label="上传商品logo" style={{borderWidth: 0}}>
          {getFieldDecorator('image', {
            initialValue: data.image,
            valuePropName: 'imageUrl',
            rules: [
              {
                required: false,
                message: '请上传商品logo',
              },
            ],
          })(
            <Upload tips="只能上传jpg/png/jpeg格式,且小于4MB" />,
          )}
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="商品说明" style={{borderWidth: 0}}>
          {getFieldDecorator('detail', {
            initialValue: data.detail,

            rules: [
              {
                required: false,
                message: '请输入商品说明',
              },
            ],
          })(
            <Editor />,
          )}
        </XFormItem>
        <div className={style['button-group']}>
          <Button htmlType="submit" type="primary" className={style['btn-save']} disabled={data.status === 2}>保存</Button>
          <Button onClick={this.props.onClose}>取消</Button>
        </div>


      </Form>
    );
  }
}
export default DrawerForm;
