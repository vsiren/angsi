import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message, Descriptions, Modal, Input} from 'antd';
import {getCategory, productType, productStatus} from 'utils/const';
import moment from 'moment';
import store from 'utils/store';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import {getProductItemPage, insertProductItem, modifyProductItem, offShelfProductItem, onShelfProductItem} from '@/network/productItem';
import XBack from '@/components/XBack';
import { getOrderDetail, orderRefund, orderRetry } from '@/network/order';
import order from '../order';


export default function (props) {
  const tableRef = useRef(null);
  const [orderInfo, setOrderInfo] = useState({});
  const [reason, setReason] = useState('');
  const [visible, setVisible] = useState(false);
  const [retryModalVisible, setRetryModalVisible] = useState(false);
  const [detail, setDetail] = useState({});
  const code = store.getKey('code');
  const orderStatusMap = code.orderStatusMap;
  const orderTypeMap = code.orderTypeMap;
  const rechargeAccountTypeMap = code.rechargeAccountTypeMap;
  const openRetryModal = (record) => {
    setRetryModalVisible(true);
  };
  const showModal = () => {
    setVisible(true);
  };

  // 发起退款
  const handleOk = e => {
    const hide = message.loading('正在申请退款', 0);
    orderRefund({
      orderId: orderInfo.id,
      remark: reason,
    }).then(res => {
      hide();
      if (res.success) {
        message.success('操作成功');
      } else {
        message.error(res.msg);
      }
    });
    setVisible(false);
  };

  const handleCancel = e => {
    console.log(e);
    setVisible(false);
  };
  const reasononChange = e => {
    setReason(e.target.value);
  };
  // 发起重试
  const handleOkRetry = () => {
    const hide = message.loading('正在申请重试', 0);

    orderRetry({
      orderId: orderInfo.id,
      remark: reason,
    }).then(res => {
      hide();
      if (res.success) {
        message.success('操作成功');
      } else {
        message.error(res.msg);
      }
    });
    setRetryModalVisible(false);
  };
  const handleCancelRetry = () => {
    setRetryModalVisible(false);
  };
  useEffect(() => {
    const query = props.location.query;
    const state = props.location;
    console.log('state', state);
    setOrderInfo({
      id: query.id,
      status: query.status,
      type: query.type,
      userId: query.userId,
      payAmount: query.payAmount,
      gmtCreated: query.gmtCreated,
      refundAmount: query.refundAmount,
    });
    const orderId = props.location.query.id;
    console.log('gmtCreated', decodeURIComponent(props.location.query.gmtCreated));
    getOrderDetail({
      orderId,
    }).then(res => {
      if (res.success) {
        console.log('res.data', res.data);
        setDetail(res.data);
      }
    });
  }, []);
  console.log('detail', detail.orderRefundInfoDtoList);

  return (
    <Layout title="订单详情">
      <div className={style.back}>
        <XBack />

      </div>
      <div className={style['btn-group']}>
        {
          (orderInfo.status === '6' || orderInfo.status === '11') && <Button type="primary" className={style['btn-refund']} onClick={e => showModal(e)}>退款</Button>
        }
        {
          (orderInfo.status === '6') && <Button type="primary" onClick={e => openRetryModal(e)}>重试</Button>
        }
      </div>
      <Descriptions title="订单信息">
        <Descriptions.Item label="订单号">{orderInfo.id}</Descriptions.Item>
        <Descriptions.Item label="订单状态">{orderStatusMap[orderInfo.status]}</Descriptions.Item>
        <Descriptions.Item label="订单金额">{orderInfo.payAmount}</Descriptions.Item>
        <Descriptions.Item label="用户ID">{orderInfo.userId}</Descriptions.Item>
        <Descriptions.Item label="购买方式">
          {orderTypeMap[orderInfo.type]}
        </Descriptions.Item>
        <Descriptions.Item label="订单创建时间">{orderInfo.gmtCreated}</Descriptions.Item>
      </Descriptions>
      {
          (detail.orderPayInfoDtoList || []).map(item => {
            return (
              <Descriptions title="支付信息">
                {item.payAmount !== null && <Descriptions.Item label="实际支付金额">{item.payAmount}</Descriptions.Item>}
                {item.paySerialNo !== null && <Descriptions.Item label="支付流水订单号">{item.paySerialNo}</Descriptions.Item>}
                {item.payTime !== null && <Descriptions.Item label="支付时间">{moment(item.payTime).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>}
              </Descriptions>

            );
          })
      }
      {
        (detail.orderRechargeInfoDto

            && (
            <Descriptions title="直充信息" key={detail.orderRechargeInfoDto.tradeNo}>
              {detail.orderRechargeInfoDto.tradeNo !== null && <Descriptions.Item label="供应商订单号">{detail.orderRechargeInfoDto.tradeNo}</Descriptions.Item> }
              {detail.orderRechargeInfoDto.rechargeTime !== null && <Descriptions.Item label="直充完成时间">{moment(detail.orderRechargeInfoDto.rechargeTime).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item> }
            </Descriptions>
            )

        )
      }
      {
          (detail.orderRechargeRetryInfoDtoList || []).map(item => {
            return (
              <Descriptions title="重试信息" key={item.tradeNo}>
                {item.gmtCreated !== null && <Descriptions.Item label="发起时间">{moment(item.gmtCreated).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>}
                {item.remark !== null && <Descriptions.Item label="备注">{item.remark}</Descriptions.Item>}
                {item.tradeNo !== null && <Descriptions.Item label="供应商订单号">{item.tradeNo}</Descriptions.Item>}

              </Descriptions>

            );
          })
      }
      {
          (detail.orderRefundInfoDtoList || []).map(item => {
            return (
              <Descriptions title="退款信息" key={item.outRefundNo}>
                {item.outRefundNo !== null && <Descriptions.Item label="退款流水订单号">{item.outRefundNo}</Descriptions.Item>}
                {item.refundTime !== null && <Descriptions.Item label="退款时间">{moment(item.refundTime).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>}
                {item.remark !== null && <Descriptions.Item label="备注">{item.remark}</Descriptions.Item>}
                {item.refundAmount !== null && <Descriptions.Item label="退款金额">{item.refundAmount}</Descriptions.Item>}
              </Descriptions>
            );
          })
      }


      {
        (detail.orderProductInfoDtoList || []).map(item => {
          return (
            <Descriptions title={item.productName}>
              {item.productId !== null && <Descriptions.Item label="商品ID">{item.productId}</Descriptions.Item>}
              {item.productName !== null && <Descriptions.Item label="商品名称">{item.productName}</Descriptions.Item>}
              {item.productItemName !== null && <Descriptions.Item label="规格名称">{item.productItemName}</Descriptions.Item>}
              {item.rechargeAccount !== null && <Descriptions.Item label="充值账号">{item.rechargeAccount}</Descriptions.Item>}
              {item.rechargeAccountType !== null && <Descriptions.Item label="充值账号类型">{rechargeAccountTypeMap[item.rechargeAccountType]}</Descriptions.Item>}
              {item.originalPrice !== null && <Descriptions.Item label="原价">{item.originalPrice}</Descriptions.Item>}
              {item.payAmount !== null && <Descriptions.Item label="折后价">{item.payAmount}</Descriptions.Item>}
              {item.supplierName !== null && <Descriptions.Item label="供应商名称">{item.supplierName}</Descriptions.Item>}
              {item.goodsSku !== null && <Descriptions.Item label="商品SKU">{item.goodsSku}</Descriptions.Item>}
              {item.cardNo !== null && <Descriptions.Item label="卡号">{item.cardNo}</Descriptions.Item>}
              {item.cardSecret !== null && <Descriptions.Item label="兑换码">{item.cardSecret}</Descriptions.Item>}
              {item.expiredTime !== null && <Descriptions.Item label="有效期">{moment(item.expiredTime).format('YYYY-MM-DD HH:mm:ss')}</Descriptions.Item>}
            </Descriptions>
          );
        })
      }
      <Modal
        title="退款"
        visible={visible}
        onOk={handleOk}
        onCancel={handleCancel}
        okText="确定"
        cancelText="取消"
      >
        <h3>确定对订单ID：{orderInfo.id}发起退款？</h3>
        <h3>退款金额为￥{orderInfo.refundAmount}</h3>
        <Input placeholder="备注：退款详情（可为空）" onChange={reasononChange} />
      </Modal>

      <Modal
        title="重试"
        visible={retryModalVisible}
        onOk={handleOkRetry}
        onCancel={handleCancelRetry}
        okText="确定"
        cancelText="取消"
      >
        <React.Fragment>
          <h3>确定对订单ID：{orderInfo.id}发起充值重试？</h3>
          <Input placeholder="备注：重试详情（可为空）" onChange={reasononChange} />

        </React.Fragment>
      </Modal>
    </Layout>
  );
}
