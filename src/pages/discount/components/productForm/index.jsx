import React, {Fragment, Component} from 'react';
import {Form, Input, Select, Button, Modal, Drawer, Radio, message} from 'antd';
import Layout from '@/components/Layout';
import XFormItem from '@/components/XFormItem';
import CustomSelect from '@/components/CustomSelect';
import style from './index.less';
import DiscountCardList from '../DiscountCardList';
import {getConsoleCode} from '@/network/product';
import {getProductMembershipItem} from '@/network/membership';


class DrawerForm extends Component {
  state = {
    detail: {
      memberCardConsoleDTOList: [],
      membershipCardMap: {},
    },
  };

  componentDidMount() {
    getProductMembershipItem({
      id: this.props.data.productId,
    }).then(res => {
      if (res.success) {
        this.setState({
          detail: res.data,
        });
      }
    });
    getConsoleCode().then(res => {
      if (res.success) {
        this.setState({

          membershipCardMap: res.data.membershipCardMap || {},
        });
      } else {
        message.error(res.msg);
      }
    });
  }

  objToArr = obj => {
    const arr = [];
    for (const i in obj) {
      arr.push({
        value: parseInt(i),
        label: obj[i],
      });
    }
    return arr;
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (err) {
        console.log('err,', err);
      }
      values = this.handleData(values);
      console.log('values', values);
      const formValues = {
        productId: this.props.data.productId,
        memberCardConsoleDTOList: values.memberCardConsoleDTOList
      }
      // todo
      this.props.onSubmit(formValues);
    });
  };

  handleData = values => {
    values.memberCardConsoleDTOList = values.memberCardConsoleDTOList.map(item => {
      const newItem = item.memberCardItemConsoleDTOList.map(spItem =>{
        spItem = values[`memberCardConsoleItemDTOList-${item.membershipId}-${spItem.productItemId}`];
        console.log('spItem',spItem);
        return spItem;
      })
      console.log('newItem',newItem)
      return {
        ...item,
        maxPurchaseAmount: parseFloat(values[item.membershipId]),
        memberCardItemConsoleDTOList: newItem
      }
    })
    // Object.keys(values).forEach(item => {
    //   console.log('item', item);
    //   if (item.indexOf('memberCardConsoleItemDTOList') !== -1) {
    //     const itemArr = item.split('-');
    //     debugger;
    //     const editMembershipId = parseInt(itemArr[1]);
    //     const editproductItemId = parseInt(itemArr[2]);
    //     const updateList = values.memberCardConsoleDTOList.find(it => it.membershipId === editMembershipId);
    //     let oldItem = updateList.memberCardItemConsoleDTOList.find(updateListItem => updateListItem.productItemId === editproductItemId);
    //     oldItem = values[item];
    //   }
    // });
    return values;
  };

  render() {
    const {getFieldDecorator} = this.props.form;
    const {membershipCardMap, detail} = this.state;
    console.log('detail', detail);
    const data = this.props.data || {};
    return (
      <Form onSubmit={this.onSubmit}>
        <XFormItem labelWidth={120} width={400} label="商品名称">
          <div>{data.name}</div>
        </XFormItem>
        <XFormItem labelWidth={120} width={400} label="会员折扣" style={{borderWidth: 0}}>
          {getFieldDecorator('memberCardConsoleDTOList', {
            initialValue: this.state.detail.memberCardConsoleDTOList,
            rules: [
              {
                required: true,
                message: '请输入会员折扣',
              },
            ],
          })(
            <DiscountCardList membershipCardMap={membershipCardMap} memberCardConsoleDTOList={detail.memberCardConsoleDTOList} form={this.props.form} />,
          )}
        </XFormItem>
        {
          this.state.detail.memberCardConsoleDTOList.map(item => (
            <XFormItem labelWidth={120} width={400} label={`${membershipCardMap && membershipCardMap[item.membershipId]}-累计原价上限`}>

              {getFieldDecorator(`${item.membershipId}`, {
                initialValue: item.maxPurchaseAmount,
                rules: [
                  {
                    required: true,
                    message: '请输入上限金额',
                  },
                ],
              })(
                <Input placeholder="请输入上限金额" />,
              )}
            </XFormItem>
          ))
        }


        <div className={style['button-group']}>
          <Button htmlType="submit" type="primary" className={style['btn-save']}>保存</Button>
          <Button onClick={this.props.onClose}>取消</Button>
        </div>


      </Form>
    );
  }
}
export default DrawerForm;
