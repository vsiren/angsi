import React from 'react';
import {Switch, Input} from 'antd';
import style from './index.less';

class InputAndSwitch extends React.Component {
  state = {
    value: this.props.value,
  };

  inputonChange = e => {
    this.setState({
      value: {
        ...this.state.value,
        discount: e.target.value / 100,
      },
    }, () => {
      this.props.onChange && this.props.onChange(this.state.value);
      // this.setNodeValue();
    });
  };

  switchonChange = checked => {
    console.log('checked', checked);
    this.setState({
      value: {
        ...this.state.value,
        status: checked ? 1 : 2,
      },
    }, () => {
      this.props.onChange && this.props.onChange(this.state.value);

      // this.setNodeValue();
    });
  };

  setNodeValue = () => {
    let formValue = this.props.form.getFieldValue('memberCardConsoleDTOList');
    formValue.forEach(it => {
      if (it.membershipId === this.props.membershipId) {
        it.memberCardItemConsoleDTOList.forEach(item => {
          if (item.productItemId === this.state.value.productItemId) {
            console.log('item------', item, this.state.value);
            item = this.state.value;
          }
        });
      }
    });

    console.log('setNodeValue', formValue);

    this.props.form.setFieldsValue({
      memberCardConsoleDTOList: formValue,
    });
  };

  render() {
    console.log('InputAndSwitch', this.state.value);
    const newValue= this.state.value.discount * 100;
    return (
      <div className={style['sp-item']}>
        <div className={style['card-name']}>{this.state.value.productItemName}</div>

        <Input placeholder="请输入折扣比例" value={newValue} onChange={this.inputonChange} className={style['discount-input']} />

        <span>%</span>
        <div className={style['switch']}>
          <span className={style['card-name']}>是否绑定</span>
          <Switch checked={this.state.value.status === 1} onChange={this.switchonChange} />

        </div>
      </div>
    );
  }
}
export default InputAndSwitch;
