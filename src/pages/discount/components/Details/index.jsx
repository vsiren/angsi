import React, {Fragment, Component} from 'react';
import {Form, Input, Select, Button, Modal, Drawer, Radio} from 'antd';
import Layout from '@/components/Layout';
import XFormItem from '@/components/XFormItem';
import CustomSelect from '@/components/CustomSelect';
import style from './index.less';
import Upload from '@/components/Upload';
import DiscountCardList from '../DiscountCardList';

class DrawerForm extends Component {
  render() {
    const {getFieldDecorator} = this.props.form;
    const data = this.props.data || {};
    const sp = ['商品规格1', '商品规格2'];
    const cardMap = ['金卡', '白金', '钻石'];
    return (
      <Form onSubmit={this.props.onSubmit}>
        <XFormItem labelWidth={90} width={400} label="商品名称">
          <div>Q币</div>
        </XFormItem>
        <XFormItem labelWidth={90} width={400} label="会员折扣">
          {getFieldDecorator('nickName', {
            rules: [
              {
                required: true,
                message: '请输入供应商名称',
              },
            ],
          })(
            <DiscountCardList mode="preview" cardMap={cardMap} />,
          )}
        </XFormItem>
        {
          cardMap.map(item => (
            <XFormItem labelWidth={90} width={400} label={`${item}-累计优惠上限`}>
              <div>100</div>
            </XFormItem>
          ))
        }


        <div className={style['button-group']}>
          <Button onClick={this.props.onClose}>关闭</Button>
        </div>


      </Form>
    );
  }
}
export default DrawerForm;
