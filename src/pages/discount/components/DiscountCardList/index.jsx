import React from 'react';
import {Input, Switch, Form} from 'antd';
import style from './index.less';
import InputAndSwitch from '../InputAndSwitch';
import Splist from '../Splist'

class InputGroup extends React.Component {
  state = {
    value: this.props.value,

  };

  render() {
    return (
      (this.props.memberCardConsoleDTOList || []).map((item, index) => {
        return (
          <div className={style['cardMap-item']}>
            <div className={style['card-name']}>{this.props.membershipCardMap[item.membershipId]}</div>
            {/* <div className={style['card-name']}>{item.membershipId}</div> */}

            <div className={style['sp-list']}>
              <Splist data={item} form={this.props.form}  membershipId={item.membershipId}/>
            </div>

          </div>

        );
      })

    );
  }
}
InputGroup.defaultProps = {
  membershipCardMap:{}
}
export default InputGroup;
