import React from 'react'
import {Form} from 'antd';
import InputAndSwitch from '../InputAndSwitch'

const RenderSpItem = props => (
  (props.data.memberCardItemConsoleDTOList || []).map((item, index) => {
    return (
      <Form.Item>
        {props.form.getFieldDecorator(`memberCardConsoleItemDTOList-${props.data.membershipId}-${item.productItemId}`, {
          initialValue: item,
          rules: [
            {
              required: false,
              message: '请输入会员折扣',
            },
          ],
        })(
          <InputAndSwitch form={props.form} membershipId={props.membershipId} />,
        )}
      </Form.Item>

    );
  })
);
export default RenderSpItem