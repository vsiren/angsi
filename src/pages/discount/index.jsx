import React, {Fragment, useEffect, useState, useRef} from 'react';
import {Form, Button, Popconfirm, message} from 'antd';
import Layout from '@/components/Layout';
import style from './index.less';
import Table from '@/components/CustomTable';
import Drawer from '@/components/XDrawer';
import DrawerForm from './components/productForm';
import Details from './components/Details';
import {getProductMembershipPage, configProductMembership} from '@/network/membership';
import moment from 'moment';
import {getCategory, productType, productStatus} from 'utils/const';


const EditDrawer = Form.create()((props) => {
  const onSubmit = values => {
    configProductMembership(values).then(res => {
      props.onSuccess(res);
    })
  };
  return (
    props.visible && (
      <Drawer visible={props.visible} onClose={props.onClose} title="编辑会员折扣">
        <DrawerForm form={props.form} onSubmit={onSubmit} data={props.data} onClose={props.onClose} />
      </Drawer>
    )
   
  );
});
const DetailDrawer = Form.create()((props) => {
  return (
    <Drawer visible={props.visible} onClose={props.onClose} title="查看详情">
      <Details form={props.form} data={props.data} onClose={props.onClose} />
    </Drawer>
  );
});

export default function () {
  const tableRef = useRef(null);
  const [createVisible, setCreateVisible] = useState(false);
  const [editVisible, setEditVisible] = useState(false);
  const [detailVisible, setDetailVisible] = useState(false);
  const [drawerInfo, setDrawerInfo] = useState({});
  
  const toggleEditDrawer = (record) => {
    const status = !editVisible;
    setEditVisible(status);
    setDrawerInfo(record);
  };
  const toggleDetailDrawer = (record) => {
    const status = !detailVisible;
    setDetailVisible(status);
    setDrawerInfo(record);
  };


  const columns = [
    {
      title: '商品ID',
      dataIndex: 'productId',
      key: 'productId',
    },

    {
      title: '商品名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '商品类型',
      dataIndex: 'type',
      key: 'type',
      render: text => {
        return productType[text]
      }
    },
    {
      title: '操作人',
      dataIndex: 'modifier',
      key: 'modifier',
    },
    {
      title: '操作时间',
      dataIndex: 'gmtModified',
      key: 'gmtModified',
      render: text => {
        return text ? moment(text).format('YYYY-MM-DD HH:mm:ss') : '-'
      }
    },
    {
      title: '操作',
      dataIndex: 'opt',
      key: 'opt',
      render: (text, record) => {
        return (
          <div>
            <a className={style['a-link']} onClick={() => toggleEditDrawer(record)}>配置</a>
            {/* <a className={style['a-link']} onClick={() => toggleDetailDrawer(record)}>查看详情</a> */}
          </div>
        );
      },
    },

  ];


  const onEditSuccess = res => {
    if (res.success) {
      message.success('修改成功');
      tableRef.current.search({
        pageNum: 1,
        pageSize: 10,
      });
      toggleEditDrawer();
    } else {
      message.error(res.msg);
    }
  };

  return (
    <Layout title="会员折扣管理">

      <Table
        scroll={{x: 'max-content'}}
        ref={tableRef}
        initQuery
        columns={columns}
        queryData={({pagination, searchKey}) => {
          return new Promise((resolve) => {
            getProductMembershipPage({
              ...pagination,
              ...searchKey,
            }).then(res => {
              if (res.success) {
                resolve({
                  data: res.data,
                  total: res.total,
                });
              }
            });
          });
        }}
      />
      <EditDrawer visible={editVisible} onClose={toggleEditDrawer} data={drawerInfo} onSuccess={onEditSuccess} />

      <DetailDrawer visible={detailVisible} onClose={toggleDetailDrawer} data={drawerInfo} />

    </Layout>
  );
}
