export const getCategory = {
  1: '腾讯尊享',
  2: '影音会员',
  3: '餐饮优惠',
  4: '生活服务',
};
export const productType = {
  1: '直冲',
  2: '卡密',
};
export const productStatus = {
  1: '待上架',
  2: '已上架',
  3: '已下架',
};
export const categoryList = [{
  value: 1,
  label: '腾讯尊享',
}, {
  value: 2,
  label: '影音会员',
}, {
  value: 3,
  label: '餐饮优惠',
}, {
  value: 4,
  label: '生活服务',
}];
