import getEnv from './env';

export default function () {
  const env = getEnv();
  let loginURL = '';
  if (env == 'local') {
    loginURL = 'https://sso-test.xx.com';
  } else if (env == 'dev') {
    loginURL = 'https://sso-test.xx.com';
  } else if (env == 'uat') {
    loginURL = 'https://sso-uat.xx.com';
  } else if (env == 'pre') {
    loginURL = 'https://sso-pre.xx.com';
  } else if (env == 'test') {
    loginURL = 'https://sso-test.xx.com';
  } else if (env == 'prd') {
    loginURL = 'https://sso.xx.io';
  }
  return loginURL;
}
