user: admin
pw:   123456

文件夹说明
> 文件名英文不用英文

root
  ├──dist 编译结果
  ├──mock ajax mock 数据
  ├──src
      ├──assets 公共资源文件夹
      ├──components  公共组件 
      ├──layouts     页面布局
      ├──less        公用less
      ├──moduels     dva 模块
      ├──network     用于ajax请求
      ├──pages       页面文件
            ├── discount    折扣页面
                ├──components   放当前页面的组件
                ├──index.jsx    折扣页面
                ├──index.less   折扣less
            ├── login    登陆页面
                ├──components   登陆专用组件
                ├──models       登陆模块
                ├──services     登陆services
                ├──index.jsx    登陆页面                    
      ├──services     全局serices
      ├──app.js       根入口文件
      ├──global.css   全局css
  ├──utils      常用工具

# httpRequest
> 1.src/services/user.js

```jsx
import request from '@/utils/request';  //引入请求

export function login(data) {           //发起post请求
  return request({
    url: '/api/login',
    method: 'POST',
    data,
  });
}
```

2.src/models/user.js

```jsx
import * as services from '@/services/user'; // 引入请求

export default {
  namespace: 'user',                        // namespace
  state: {},                                // state默认值
  reducers: {                               // 定义reducers
    save(state, { payload: { data: list, total } }) {    // 定义save reducers
      return { ...state, list, total };
    },
  },
  effects: {
    // 定义了fetch 获取数据后 put 触发reducers
    *fetch({ payload: { page } }, { call, put }) {
      const { data, headers } = yield call(usersService.fetch, { page });
      yield put({ type: 'save', payload: { data, total: headers['x-total-count'] } });
    },
  },
  subscriptions: {      // 订阅当发生变化时触发dispatch
    setup({ dispatch, history }) {
      return history.listen(({ pathname, query }) => {
        if (pathname === '/users') {
          dispatch({ type: 'fetch', payload: query });
        }
      });
    },
  }
};
```

3.connect 链接起来
接受dispatch
method 触发 dispatch namespace/reducers|fetch

```jsx
import React from 'react';
import { connect } from 'dva';
import ProductList from '../components/ProductList';

const Products = ({ dispatch, products }) => {
  function handleDelete(id) {
    dispatch({
      type: 'user/save',
      payload: id,
    });
  }
  return (
    <div>
      <h2>List of Products</h2>
      <ProductList onDelete={handleDelete} products={products} />
    </div>
  );
};

const mapStateToProps = state => ({
  user: state.user
})
const mapDispatchToProps = (dispatch) => {
  return {
    changeSex: (url) => dispatch({
      type: 'home/setState',
      payload: '11111111',
    })
  }
}
export default connect(mapStateToProps)(Products)
```





 > dva 用法
react（https://react.docschina.org/）
umi--路由配置（https://umijs.org/）
dva--数据流方案（https://dvajs.com/guide/）
